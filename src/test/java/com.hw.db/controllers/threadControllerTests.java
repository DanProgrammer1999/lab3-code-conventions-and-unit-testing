package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class threadControllerTests {
    private Thread thread;

    private static final threadController threadController = new threadController();
    private static final MockedStatic<ThreadDAO> threadDaoMock = Mockito.mockStatic(ThreadDAO.class);
    private static final MockedStatic<UserDAO> userDaoMock = Mockito.mockStatic(UserDAO.class);

    private static final List<Post> postList = Collections.emptyList();
    private static final int votesCount = 9;

    @BeforeEach
    @DisplayName("Thread controller test")
    void setUp() {
        thread = new Thread(
                "admin",
                new Timestamp(System.currentTimeMillis()),
                "forum",
                "msg",
                "slug",
                "title",
                votesCount);

        threadDaoMock.reset();
        userDaoMock.reset();
    }

    @AfterAll
    static void tearDown(){
        threadDaoMock.close();
        userDaoMock.close();
    }

    @Test
    @DisplayName("Check id or slug: id test")
    void checkIdOrSlugIdTest() {
        var id = 42;
        threadDaoMock.when(() -> ThreadDAO.getThreadById(id)).thenReturn(thread);
        assertEquals(thread, threadController.CheckIdOrSlug(id + ""));
        assertNull(threadController.CheckIdOrSlug("24"));
    }

    @Test
    @DisplayName("Check id or slug: slug test")
    void checkIdOrSlugSlugTest() {
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        assertEquals(thread, threadController.CheckIdOrSlug("slug"));
        assertNull(threadController.CheckIdOrSlug("not-slug"));
    }

    @Test
    @DisplayName("Create post")
    void createPostTest() {
        assertEquals(
                ResponseEntity.status(HttpStatus.CREATED).body(postList),
                threadController.createPost("slug", postList)
        );
    }

    @Test
    @DisplayName("Get posts")
    void postsTest() {
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        assertEquals(
                ResponseEntity.status(HttpStatus.OK).body(postList),
                threadController.Posts("slug", 1, 0, "sort", false)
        );
    }

    @Test
    @DisplayName("Change test")
    void changeTest() {
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        assertThrows(NullPointerException.class, () -> threadController.change("slug", thread));
    }

    @Test
    void infoTest() {
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.info("slug"));
    }

    @Test
    void createVoteTest() {
        var user = new User("admin", "test@example.com", "Test Test", "Nothing special");
        var vote = new Vote("admin", 1);

        userDaoMock.when(() -> UserDAO.Info("admin")).thenReturn(user);
        threadDaoMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
        assertEquals(
                ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(),
                threadController.createVote("slug", vote).getStatusCode()
        );
    }
}
